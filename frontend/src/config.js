export const BASE_URL = 'http://localhost:7000';
export const ARTISTS_URL = '/artists';
export const ALBUMS_URL = '/albums';
export const TRACKS_URL = '/tracks';
export const USERS_URL = '/users';
export const USER_FC_LOGIN_URL = '/users/facebookLogin';
export const POSTS_URL = '/posts';
export const COMMENTS_URL = '/comments';
export const USERS_SESSIONS_URL = '/users/sessions';
export const TRACK_HISTORY_URL = '/tracksHistories';
export const IMAGES_URL = BASE_URL + '/';

export const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;