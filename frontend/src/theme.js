import {createTheme} from "@mui/material/styles";

export const theme = createTheme({
    palette: {
        customBlack: {
            main: "#13167a",
            contrastText: "#ffffff",
        }
    },
    components: {
        MuiTextField: {
            styleOverrides: {
                root: {
                    width: '100%',
                    backgroundColor: 'white',
                }
            }
        }
    },
});