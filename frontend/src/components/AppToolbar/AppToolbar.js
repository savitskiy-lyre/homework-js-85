import React from 'react';
import {AppBar, Button, Divider, Grid, Stack, Toolbar} from "@mui/material";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import ProfileMenu from "../ProfileMenu/ProfileMenu";
import {userLogout} from "../../store/actions/music/auth/profileActions";
import {fetchTrackHistorySuccess} from "../../store/actions/music/auth/trackHistoryActions";

const AppToolbar = () => {
    const profile = useSelector((state) => state.profile.profile);
    const dispatch = useDispatch();

    return (
        <>
            <AppBar position={"fixed"} color={'customBlack'}>
                <Toolbar>
                    <Grid container justifyContent={"space-between"} alignItems={"center"}>
                        <Grid item>
                            <Stack flexDirection={'row'}>
                                <Button component={Link}
                                        to='/'
                                        sx={{color: "inherit"}}
                                >
                                    Music
                                </Button>
                                <Button component={Link}
                                        to='/posts'
                                        sx={{color: "inherit"}}
                                >
                                    Forum
                                </Button>
                            </Stack>
                        </Grid>

                        {profile ? (
                            <ProfileMenu
                                handleLogout={() => {
                                    dispatch(userLogout());
                                    dispatch(fetchTrackHistorySuccess(null));
                                }}
                                username={profile.username}
                                locationTo={'/trackHistory'}
                                avatar={profile.image}
                            />
                        ) : (
                            <Grid display={"inline-flex"}>
                                <Grid item>
                                    <Button component={Link}
                                            to='/signin'
                                            sx={{color: "inherit"}}
                                    >
                                        Sign in
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Divider orientation="vertical" sx={{backgroundColor: 'gainsboro',}}/>
                                </Grid>
                                <Grid item>
                                    <Button component={Link}
                                            to='/signup'
                                            sx={{color: "inherit"}}
                                    >
                                        Sign up
                                    </Button>
                                </Grid>
                            </Grid>
                        )
                        }
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </>
    );
};

export default AppToolbar;