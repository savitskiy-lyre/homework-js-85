import React from 'react';
import {Grid, Skeleton} from "@mui/material";

const AlbumsSkeleton = () => {
    return (
        <Grid item container justifyContent={"center"} spacing={2}>
            <Grid item>
                <Skeleton variant="rectangular" width={210} height={118} />
            </Grid>
            <Grid item>
                <Skeleton variant="rectangular" width={210} height={118} />
            </Grid>
            <Grid item>
                <Skeleton variant="rectangular" width={210} height={118} />
            </Grid>
        </Grid>
    );
};

export default AlbumsSkeleton;