import React from 'react';
import {Grid, Skeleton} from "@mui/material";

const ArtistsSkeleton = () => {
    return (
        <Grid item container justifyContent={"center"} spacing={2}>
            <Grid item>
                <Skeleton variant="text"/>
                <Skeleton variant="circular" width={40} height={40}/>
                <Skeleton variant="rectangular" width={210} height={118}/>
            </Grid>
            <Grid item>
                <Skeleton variant="text"/>
                <Skeleton variant="circular" width={40} height={40}/>
                <Skeleton variant="rectangular" width={210} height={118}/>
            </Grid>
            <Grid item>
                <Skeleton variant="text"/>
                <Skeleton variant="circular" width={40} height={40}/>
                <Skeleton variant="rectangular" width={210} height={118}/>
            </Grid>
        </Grid>
    );
};

export default ArtistsSkeleton;