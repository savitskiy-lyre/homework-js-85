import React from 'react';
import {Grid, Skeleton} from "@mui/material";

const TracksSkeleton = () => {
    return (
        <>
            <Grid item>
                <Skeleton />
            </Grid>
            <Grid item>
                <Skeleton />
            </Grid>
            <Grid item>
                <Skeleton />
            </Grid>
            <Grid item>
                <Skeleton />
            </Grid>
            <Grid item>
                <Skeleton />
            </Grid>
        </>
    );
};

export default TracksSkeleton;