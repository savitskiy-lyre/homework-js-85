import React from 'react';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

const ItemContextMenu = ({children, menuItems}) => {

    const [contextMenu, setContextMenu] = React.useState(null);

    const handleContextMenu = (event) => {
        event.preventDefault();
        setContextMenu(
            contextMenu === null
                ? {
                    mouseX: event.clientX - 2,
                    mouseY: event.clientY - 4,
                }
                :
                null,
        );
    };

    const handleClose = () => {
        setContextMenu(null);
    };
    return (<div onContextMenu={handleContextMenu} style={{cursor: 'context-menu'}}>
            {children}
            <Menu
                open={contextMenu !== null}
                onClose={handleClose}
                anchorReference="anchorPosition"
                anchorPosition={
                    contextMenu !== null
                        ? {top: contextMenu.mouseY, left: contextMenu.mouseX}
                        : undefined
                }
            >
                {menuItems && (
                    menuItems.map(item => {
                        return (
                            <MenuItem
                                key={item._id}
                                onClick={() => {
                                    item.menuHandler()
                                    handleClose()
                                }}
                            >
                                {item.title}
                            </MenuItem>
                        )
                    })
                )}
            </Menu>
        </div>
    );
};

export default ItemContextMenu;