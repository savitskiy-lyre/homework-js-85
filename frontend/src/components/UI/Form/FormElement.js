import React from 'react';
import PropTypes from 'prop-types';
import {Grid, MenuItem, TextField} from "@mui/material";

const FormElement = ({
                         label,
                         name,
                         value,
                         onChange,
                         required,
                         error,
                         autoComplete,
                         type,
                         select,
                         options,
                         multiline,
                         rows
                     }) => {
    let inputChildren = null;

    if (select) {
        if (options.data) {
            inputChildren = options.data.map(option => (
                <MenuItem
                    key={option._id}
                    value={option._id}
                >
                    {option[options.key]}
                </MenuItem>)
            );
        } else {
            inputChildren = (
                <MenuItem
                    value={''}
                >
                    None:
                </MenuItem>
            )
        }

    }
    return (
        <Grid item xs={12}>
            <TextField
                label={label}
                name={name}
                value={value}
                onChange={onChange}
                type={type}
                error={Boolean(error)}
                helperText={error}
                select={select}
                multiline={multiline}
                rows={rows}
                autoComplete={autoComplete}
                required={required}
            >
                {inputChildren}
            </TextField>
        </Grid>
    );
};

FormElement.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
    type: PropTypes.string,
    select: PropTypes.bool,
    options: PropTypes.object,
    multiline: PropTypes.bool,
    rows: PropTypes.number
};

export default FormElement;