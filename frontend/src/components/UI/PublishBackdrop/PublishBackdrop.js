import * as React from 'react';
import {Tooltip} from "@mui/material";

const PublishBackdrop = () => (
    <Tooltip title={'Not published'} placement="bottom">
        <div style={{
            position: 'absolute',
            left: '0',
            top: '0',
            width: '100%',
            height: '100%',
            background: 'linear-gradient(111deg, rgba(199,199,199,0.2) 14%, rgba(158,158,158,0.4) 15%, rgba(199,199,199,0.2) 16%, rgba(199,199,199,0.2) 38%, rgba(158,158,158,0.4) 39%, rgba(199,199,199,0.2) 40%, rgba(199,199,199,0.2) 60%, rgba(158,158,158,0.4) 61%, rgba(199,199,199,0.2) 62%, rgba(199,199,199,0.2) 85%, rgba(158,158,158,0.4) 86%, rgba(199,199,199,0.2) 87%)',
            zIndex: '1000',
        }}/>
    </Tooltip>
);

export default PublishBackdrop;

