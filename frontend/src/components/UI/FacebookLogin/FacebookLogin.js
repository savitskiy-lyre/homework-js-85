import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "@mui/material";
import FacebookIcon from '@mui/icons-material/Facebook';
import {useDispatch} from "react-redux";
import {facebookLogin} from "../../../store/actions/music/auth/profileActions";
import {facebookAppId} from "../../../config";

const FacebookLogin = () => {
    const dispatch = useDispatch();
    const responseFacebook = (response) => {
        dispatch(facebookLogin(response));
    };
    return (
        <FacebookLoginButton
            appId={facebookAppId}
            fields="name,email,picture"
            callback={responseFacebook}
            render={props => {
                return (
                    <Button
                        color={'secondary'}
                        variant={"outlined"}
                        fullWidth
                        onClick={props.onClick}
                        startIcon={<FacebookIcon/>}

                    >
                        Login with facebook
                    </Button>
                )}}
        />
    );
};

export default FacebookLogin;