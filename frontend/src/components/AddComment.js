import React, {useState} from 'react';
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import ForumIcon from "@mui/icons-material/Forum";
import Typography from "@mui/material/Typography";
import {Stack} from "@mui/material";
import TextField from "@mui/material/TextField";
import LoadingButton from "@mui/lab/LoadingButton";

const AddComment = ({onCommentCreate, loading, postId}) => {
    const [state, setState] = useState({
        text: '',
    })

    const onSubmit = (e) => {
        e.preventDefault();
        onCommentCreate({...state, post: postId});
        setState(prevState => ({...prevState, text: ''}));
    }

    return (
        <Box
            mt={6}
            textAlign={'center'}
        >
            <Box
                sx={{
                    display: 'inline-flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                width={'100%'}
                maxWidth={'500px'}
            >
                <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                    <ForumIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add new comment
                </Typography>
                <Stack
                    component="form"
                    spacing={4}
                    sx={{mt: 1}}
                    width={'100%'}
                    onSubmit={onSubmit}
                >
                    <TextField
                        label="Text"
                        name="text"
                        value={state.text}
                        onChange={(e) => setState(prevState => ({...prevState, text: e.target.value}))}
                        required
                    />
                    <LoadingButton
                        type="submit"
                        loading={loading}
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Submit
                    </LoadingButton>
                </Stack>
            </Box>
        </Box>
    );
};

export default AddComment;