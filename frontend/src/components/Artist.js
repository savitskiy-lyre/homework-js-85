import React from 'react';
import {CardActionArea} from "@mui/material";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import {Link as RouterLink} from "react-router-dom";
import Card from "@mui/material/Card";

const Artist = ({photo, name, _id, children}) => {
    return (
        <div>
            <Card sx={{maxWidth: 345}}>
                <CardActionArea component={RouterLink} to={'/albums?artist=' + _id} sx={{position: "relative"}}>
                    <CardMedia
                        component="img"
                        height="140"
                        image={photo}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {name}
                        </Typography>
                        {children}
                    </CardContent>
                </CardActionArea>
            </Card>
        </div>
    );
};

export default Artist;