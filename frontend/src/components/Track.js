import React from 'react';
import {CardActionArea, Grid, Paper, Typography} from "@mui/material";

const Track = ({title, duration, count, handleClick, children}) => {
    return (
        <Paper>
            <CardActionArea onClick={handleClick}>
                <Grid container justifyContent={"space-between"} spacing={2} p={1}>
                    <Grid item>
                        <Typography fontSize={19}>
                            <strong style={{fontSize: '13px'}}>№{count} </strong> {title}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography>
                            {duration}
                        </Typography>
                    </Grid>
                    {children}
                </Grid>
            </CardActionArea>
        </Paper>
    );
};

export default Track;