import React from 'react';
import Card from "@mui/material/Card";
import {Link as RouterLink} from "react-router-dom";
import {Box, CardActionArea} from "@mui/material";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";

const Album = ({image, locationTo, date, title, children}) => {
    return (
        <Card>
            <CardActionArea component={RouterLink} to={locationTo} sx={{display: 'flex'}}>
                <Box sx={{display: 'flex', flexDirection: 'column'}}>
                    <CardContent sx={{flex: '1 0 auto'}}>
                        <Typography component="div" variant="h5">
                            {title}
                        </Typography>
                        <Typography variant="subtitle1" color="text.secondary" component="div">
                            {date}
                        </Typography>
                        {children}
                    </CardContent>
                </Box>
                <CardMedia
                    component="img"
                    sx={{width: 151}}
                    image={image}
                    alt="pic"
                />
            </CardActionArea>
        </Card>
    );
};

export default Album;