import * as React from 'react';
import {Route, Switch} from "react-router-dom";
import Artists from "./containers/Artists";
import Albums from "./containers/Albums";
import Layout from "./components/UI/Layout/Layout";
import Tracks from "./containers/Tracks";
import SignUp from "./containers/SignUp";
import SignIn from "./containers/SignIn";
import TrackHistory from "./containers/Trackhistory/TrackHistory";
import Posts from "./containers/Posts";
import AddPost from "./containers/AddPost/AddPost";
import Post from "./containers/Post";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddTrack from "./containers/AddTrack/AddTrack";
import ProtectedRoute from "./components/UI/ProtectedRoute/ProtectedRoute";
import {useSelector} from "react-redux";

const App = () => {
    const profile = useSelector(state => state.profile.profile)

    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists}/>
                <ProtectedRoute
                    path="/artists/add"
                    component={AddArtist}
                    isAllowed={profile?.role === 'admin' || profile?.role === 'user'}
                    redirectTo="/signin"
                />
                <Route path="/artists" component={Artists}/>
                <ProtectedRoute
                    path="/posts/add"
                    component={AddPost}
                    isAllowed={profile?.role === 'admin' || profile?.role === 'user'}
                    redirectTo="/signin"
                />
                <Route path="/posts/:id" component={Post}/>
                <Route path="/posts" component={Posts}/>
                <ProtectedRoute
                    path="/albums/add"
                    component={AddAlbum}
                    isAllowed={profile?.role === 'admin' || profile?.role === 'user'}
                    redirectTo="/signin"
                />
                <Route path="/albums" component={Albums}/>
                <ProtectedRoute
                    path="/tracks/add"
                    component={AddTrack}
                    isAllowed={profile?.role === 'admin' || profile?.role === 'user'}
                    redirectTo="/signin"
                />
                <Route path="/tracks" component={Tracks}/>
                <Route path="/signin" component={SignIn}/>
                <Route path="/signup" component={SignUp}/>
                <Route path="/trackHistory" component={TrackHistory}/>
            </Switch>
        </Layout>
    );
};

export default App;
