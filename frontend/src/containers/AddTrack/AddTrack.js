import React, {useEffect, useState} from 'react';
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import ForumIcon from "@mui/icons-material/Forum";
import Typography from "@mui/material/Typography";
import {Stack} from "@mui/material";
import FormElement from "../../components/UI/Form/FormElement";
import LoadingButton from "@mui/lab/LoadingButton";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtistAlbums} from "../../store/actions/music/noAuth/albumsActions";
import {fetchArtists} from "../../store/actions/music/noAuth/artistsActions";
import {addTrack, addTrackFailure} from "../../store/actions/music/noAuth/tracksActions";

const AddTrack = () => {
    const dispatch = useDispatch();
    const formErr = useSelector(state => state.tracks.addFormErr);
    const loading = useSelector(state => state.tracks.addFormBtnLoading);
    const artists = useSelector(state => state.artists.artists);
    const albums = useSelector(state => state.albums.albums);

    const [state, setState] = useState({
        title: '',
        album: '',
        duration: '',
        count: '',
    });

    const [author, setAuthor] = useState('');

    console.log(state);

    const getErrorMessage = field => {
        return formErr?.errors[field]?.message;
    }

    const formSubmit = (e) => {
        e.preventDefault();
        dispatch(addTrack(state));
    };
    const inputChanger = (e) => {
        if (!state.title) dispatch(addTrackFailure(null));
        if (!state.album) dispatch(addTrackFailure(null));
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);
    useEffect(() => {
        if (author) {
            dispatch(fetchArtistAlbums('?artist=' + author))
        }
    }, [dispatch, author]);

    return (
        <Box
            mt={6}
            textAlign={'center'}
        >
            <Box
                sx={{
                    display: 'inline-flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                width={'100%'}
                maxWidth={'500px'}
            >
                <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                    <ForumIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add new track
                </Typography>
                <Stack
                    component="form"
                    noValidate
                    spacing={4}
                    sx={{mt: 1}}
                    width={'100%'}
                    onSubmit={formSubmit}
                >
                    <FormElement
                        label={'Title'}
                        name={'title'}
                        onChange={inputChanger}
                        value={state.title}
                        error={getErrorMessage('title')}
                        required
                    />
                    <FormElement
                        select
                        label={'Author'}
                        name={'author'}
                        onChange={(e) => {
                            if (!author) dispatch(addTrackFailure(null));
                            setAuthor(e.target.value);
                        }}
                        value={author}
                        error={getErrorMessage('author') ? 'Choose Author pls !!!' : null}
                        options={{
                            data: artists,
                            key: 'name',
                        }}
                        required
                    />
                    <FormElement
                        select
                        label={'Album'}
                        name={'album'}
                        onChange={inputChanger}
                        value={state.album}
                        error={getErrorMessage('author') ? 'Choose Album pls !!!' : null}
                        options={{
                            data: albums,
                            key: 'title',
                        }}
                        required
                    />
                    <FormElement
                        label={'Duration'}
                        name={'duration'}
                        onChange={inputChanger}
                        value={state.duration}
                    />
                    <FormElement
                        label={'Count'}
                        name={'count'}
                        onChange={inputChanger}
                        value={state.count}
                    />
                    <LoadingButton
                        type="submit"
                        loading={loading}
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Submit
                    </LoadingButton>
                </Stack>
            </Box>
        </Box>
    );
};

export default AddTrack;