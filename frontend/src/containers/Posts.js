import React, {useEffect} from 'react';
import {Link as RouterLink} from "react-router-dom";
import {Button, Grid, Link, Stack, SvgIcon, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../store/actions/forum/auth/postsActions";
import AlbumsSkeleton from "../components/UI/AlbumsSkeleton";
import NotFoundContent from "../components/UI/NotFoundContent/NotFoundContent";
import Box from "@mui/material/Box";
import CardMedia from "@mui/material/CardMedia";
import MessageIcon from '@mui/icons-material/Message';
import {IMAGES_URL} from "../config";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles({
    post: {
        background: 'white',
        border: '2px solid gainsboro',
        borderRadius: '6px',
    }
})

const Posts = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const posts = useSelector(state => state.posts.data);
    const profile = useSelector(state => state.profile.profile);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch])
    return (
        <Stack spacing={5}>
            {profile && (
                <Button
                    component={RouterLink}
                    to={'posts/add'}
                >
                    Add
                </Button>
            )}
            {!posts && (
                <Grid container spacing={8} my={5} direction={"column"}>
                    <AlbumsSkeleton/>
                    <AlbumsSkeleton/>
                    <AlbumsSkeleton/>
                </Grid>
            )}
            {posts?.length > 0 && (
                <Stack spacing={4} mt={5}>
                    {posts.map((post) => {
                        return (
                            <Grid container className={classes.post} key={post._id}>
                                <Grid item sx={{borderRight: '1px solid gainsboro'}}>
                                    {post.image ? (
                                        <CardMedia
                                            component="img"
                                            sx={{width: 151}}
                                            image={IMAGES_URL + post.image}
                                            alt="pic"
                                        />
                                    ) : (
                                        <SvgIcon
                                            sx={{
                                                width: 151,
                                                height: 90,
                                                padding: '10px 0',
                                            }}
                                            fontSize={'large'}

                                        >
                                            <MessageIcon/>
                                        </SvgIcon>
                                    )}

                                </Grid>
                                <Grid item textAlign={"center"} flexGrow={1} m={2}>
                                    <Stack spacing={1}>
                                        <Typography
                                            variant={"subtitle1"}
                                            textAlign={"center"}
                                        >
                                            {post.datetime} created by {post.user.username}
                                        </Typography>
                                        <Link component={RouterLink}
                                              to={'/posts/' + post._id}
                                        >
                                            {post.title}...
                                        </Link>
                                    </Stack>
                                </Grid>
                            </Grid>
                        );
                    })}
                </Stack>
            )}
            {posts?.length === 0 && (
                <>
                    {profile && (
                        <Button component={RouterLink} to={'posts/add'} color={"inherit"}
                        >
                            Add
                        </Button>
                    )}
                    <Box>
                        <NotFoundContent
                            name={'posts'}
                        />
                    </Box>
                </>

            )}
        </Stack>
    );
};

export default Posts;