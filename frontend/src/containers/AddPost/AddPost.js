import React, {useMemo, useState} from 'react';
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import {Stack} from "@mui/material";
import LoadingButton from '@mui/lab/LoadingButton';
import ForumIcon from "@mui/icons-material/Forum";
import TextField from "@mui/material/TextField";
import {useDispatch, useSelector} from "react-redux";
import {addPost} from "../../store/actions/forum/auth/postsActions";

const AddPost = () => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.posts.submitBtnLoading);
    const [state, setState] = useState({
        title: '',
        description: '',
        image: null,
    });
    const inpErr = useMemo(() => {
        if (!state.description && (!state.image && state.image !== null)) return true;
    }, [state.description, state.image])

    const onSubmit = (e) => {
        e.preventDefault();
        if (state.image === null) {
            return setState(prevState => ({...prevState, image: undefined}));
        }
        const newPost = new FormData();
        Object.keys(state).forEach(key => {
            newPost.append(key, state[key]);
        })
        dispatch(addPost(newPost));
    };

    return (
        <Box
            mt={6}
            textAlign={'center'}
        >
            <Box
                sx={{
                    display: 'inline-flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                width={'100%'}
                maxWidth={'500px'}
            >
                <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                    <ForumIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add new post
                </Typography>
                <Stack
                    component="form"
                    spacing={4}
                    sx={{mt: 1}}
                    width={'100%'}
                    onSubmit={onSubmit}
                >
                    <TextField
                        label="Title"
                        name="title"
                        value={state.title}
                        onChange={(e) => setState(prevState => ({...prevState, title: e.target.value}))}
                        autoFocus
                        required
                    />
                    <TextField
                        label="Description"
                        name="description"
                        value={state.description}
                        onChange={(e) => setState(prevState => ({...prevState, description: e.target.value}))}
                        error={inpErr}
                        helperText={'Fill description or image field'}
                    />
                    <TextField
                        type={"file"}
                        name="image"
                        onChange={(e) => setState(prevState => ({...prevState, image: e.target.files[0]}))}
                        error={inpErr}
                        helperText={'Fill description or image field'}
                    />
                    <LoadingButton
                        type="submit"
                        loading={loading}
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Submit
                    </LoadingButton>
                </Stack>
            </Box>
        </Box>
    );
};

export default AddPost;