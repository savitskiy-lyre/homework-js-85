import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../store/actions/forum/auth/postsActions";
import {addComment, fetchComments} from "../store/actions/forum/auth/commentsActions";
import AddComment from "../components/AddComment";
import {Grid, Stack, Typography} from "@mui/material";
import {IMAGES_URL} from "../config";
import AlbumsSkeleton from "../components/UI/AlbumsSkeleton";

const Post = ({match}) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.currentPost);
    const comments = useSelector(state => state.comments.data);
    const profile = useSelector(state => state.profile.profile);
    const loading = useSelector(state => state.comments.submitBtnLoading);

    useEffect(() => {
        dispatch(fetchPost(match.params.id));
        dispatch(fetchComments(match.params.id));
    }, [dispatch, match.params.id])
    return (
        <Stack spacing={5} mt={5} textAlign={"center"}>
            {!post && !comments && (
                <Grid container spacing={8} my={5} direction={"column"}>
                    <AlbumsSkeleton/>
                    <AlbumsSkeleton/>
                    <AlbumsSkeleton/>
                </Grid>
            )}
            {post && (
                <>
                    <Typography variant={"h4"}>
                        {post.title}
                    </Typography>
                    {post?.description && (
                        <Typography variant={"body2"}>
                            {post.description}
                        </Typography>
                    )}
                    {post?.image && (
                        <Typography>
                            <img src={IMAGES_URL + post.image} alt="pic" style={{maxWidth: '100%'}}/>
                        </Typography>
                    )}
                </>
            )}
            {profile && (
                <AddComment
                    onCommentCreate={data => dispatch(addComment(data))}
                    postId={match.params.id}
                    loading={loading}
                />
            )}
            {comments?.length > 0 && (
                <Grid container flexDirection={"column"}>
                    {comments.map((comment) => {
                        return (
                            <Grid
                                item
                                container
                                key={comment._id}
                                alignItems={"center"}
                                spacing={1}
                                flexDirection={'column'}
                                justifyContent={"center"}
                                sx={{background: 'white', border: '2px solid gainsboro', borderRadius: '6px'}}
                                p={1}
                                mb={4}
                            >
                                <Grid item>
                                    <Typography variant={"h5"}>
                                        Author: {comment.user.username}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant={"subtitle2"}>
                                        {comment.text}
                                    </Typography>
                                </Grid>
                            </Grid>
                        )
                    }).reverse()}
                </Grid>
            )}
        </Stack>
    );
};

export default Post;