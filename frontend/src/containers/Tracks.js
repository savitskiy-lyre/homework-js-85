import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Grid, Stack, Typography} from "@mui/material";
import Track from "../components/Track";
import TracksSkeleton from "../components/UI/TracksSkeleton";
import {addTrackToHistory} from "../store/actions/music/auth/trackHistoryActions";
import {fetchAlbumTracks} from "../store/actions/music/noAuth/tracksActions";
import NotFoundContent from "../components/UI/NotFoundContent/NotFoundContent";
import Box from "@mui/material/Box";
import {Link as RouterLink} from "react-router-dom";
import {deleteItem, publishItem} from "../store/actions/adminActions";
import {TRACKS_URL} from "../config";
import PublishBackdrop from "../components/UI/PublishBackdrop/PublishBackdrop";
import ItemContextMenu from "../components/UI/ItemContextMenu/ItemContextMenu";

const Tracks = ({location}) => {
    const tracks = useSelector(state => state.tracks.tracks);
    const profile = useSelector(state => state.profile.profile);
    const dispatch = useDispatch();

    let handleTrackToHistory = () => {
        console.log('First, log in to add tracks to history');
    }
    if (profile) {
        handleTrackToHistory = (id) => dispatch(addTrackToHistory(id));
    }

    useEffect(() => {
        dispatch(fetchAlbumTracks(location.search));
    }, [dispatch, location.search]);

    return (
        <>
            {profile && (
                <Button
                    component={RouterLink}
                    size="large"
                    to={'tracks/add'}
                >
                    Add
                </Button>
            )}
            {!tracks && (
                <Grid container spacing={4} my={5} direction={"column"}>
                    <TracksSkeleton/>
                    <TracksSkeleton/>
                    <TracksSkeleton/>
                </Grid>
            )}
            {tracks?.length > 0 && (
                <Stack spacing={5} mt={3} width={'100%'}>
                    <Grid container justifyContent={"space-evenly"} alignItems={"center"}>
                        <Grid item>
                            <Typography variant={"h3"}>
                                Author: {tracks[0].album.author.name}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant={"h4"}>
                                Album: {tracks[0].album.title}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Typography variant={'h4'} component={'h1'}>
                        List of tracks:
                    </Typography>
                    <Stack spacing={2}>
                        {tracks.map((track) => {
                            if (!track.published && profile?.role !== 'admin') return null;
                            return (
                                <ItemContextMenu
                                    key={track._id}
                                    menuItems={profile?.role === 'admin' ? [
                                        {
                                            _id: track._id + 'publishMenu',
                                            title: track.published ? 'Unpublish' : 'Publish',
                                            menuHandler: async () => {
                                                await dispatch(publishItem(TRACKS_URL + `/${track._id}/publish`, !track.published));
                                                dispatch(fetchAlbumTracks(location.search))
                                            },
                                        }, {
                                            _id: track._id + 'deleteMenu',
                                            title: 'Delete',
                                            menuHandler: async () => {
                                                await dispatch(deleteItem(TRACKS_URL + `/` + track._id));
                                                dispatch(fetchAlbumTracks(location.search))
                                            },
                                        },
                                    ] : null}
                                >
                                    <>
                                        <Track
                                            key={track._id}
                                            title={track.title}
                                            duration={track.duration}
                                            count={track.count ? track.count : ''}
                                            handleClick={() => handleTrackToHistory(track._id)}
                                        >
                                            {!track.published && (
                                                <PublishBackdrop/>
                                            )}
                                        </Track>
                                    </>
                                </ItemContextMenu>

                            );
                        })}
                    </Stack>
                </Stack>
            )}
            {tracks?.length === 0 && (
                <Box m={'auto'}>
                    <NotFoundContent
                        name={'tracks'}
                    />
                </Box>
            )

            }
        </>
    );
};

export default Tracks;