import React, {useEffect} from 'react';
import {Button, Grid, Stack, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import Album from "../components/Album";
import {ALBUMS_URL, IMAGES_URL} from "../config";
import notFoundImg from "../assets/image/notFound.png";
import AlbumsSkeleton from "../components/UI/AlbumsSkeleton";
import NotFoundContent from "../components/UI/NotFoundContent/NotFoundContent";
import {fetchArtistAlbums} from "../store/actions/music/noAuth/albumsActions";
import Box from "@mui/material/Box";
import {Link as RouterLink} from "react-router-dom";
import {deleteItem, publishItem} from "../store/actions/adminActions";
import PublishBackdrop from "../components/UI/PublishBackdrop/PublishBackdrop";
import ItemContextMenu from "../components/UI/ItemContextMenu/ItemContextMenu";

const Albums = ({location}) => {
    const albums = useSelector(state => state.albums.albums);
    const dispatch = useDispatch();
    const profile = useSelector(state => state.profile.profile);
    useEffect(() => {
        dispatch(fetchArtistAlbums(location.search));
    }, [dispatch, location.search]);

    return (
        <>
            {profile && (
                <Button
                    component={RouterLink}
                    size="large"
                    to={'albums/add'}
                >
                    Add
                </Button>
            )}
            {!albums && (
                <Grid container spacing={8} my={5} direction={"column"}>
                    <AlbumsSkeleton/>
                    <AlbumsSkeleton/>
                    <AlbumsSkeleton/>
                </Grid>
            )}
            {albums?.length > 0 && (
                <Stack spacing={5} mt={3} width={'100%'}>
                    <Typography variant={'h3'} textAlign={"center"}>
                        Author: {albums[0].author.name}
                    </Typography>
                    <Typography variant={'h5'}>
                        List of albums:
                    </Typography>
                    <Grid container spacing={2} justifyContent={"center"}>
                        {albums.map(album => {
                            if (!album.published && profile?.role !== 'admin') return null;
                            return (
                                <Grid item key={album._id}>
                                    <ItemContextMenu
                                        menuItems={profile?.role === 'admin' ? [
                                            {
                                                _id: album._id + 'publishMenu',
                                                title: album.published ? 'Unpublish' : 'Publish',
                                                menuHandler: async () => {
                                                    await dispatch(publishItem(ALBUMS_URL + `/${album._id}/publish`, !album.published));
                                                    dispatch(fetchArtistAlbums(location.search));
                                                },
                                            }, {
                                                _id: album._id + 'deleteMenu',
                                                title: 'Delete',
                                                menuHandler: async () => {
                                                    await dispatch(deleteItem(ALBUMS_URL + `/` + album._id));
                                                    dispatch(fetchArtistAlbums(location.search));
                                                },
                                            },
                                        ] : null}
                                    >
                                        <>
                                            <Album
                                                title={album.title}
                                                locationTo={`/tracks?album=${album._id}`}
                                                date={album.date}
                                                image={album.image ? IMAGES_URL + album.image : notFoundImg}
                                            >
                                                {!album.published && (
                                                    <PublishBackdrop/>
                                                )}
                                            </Album>
                                        </>
                                    </ItemContextMenu>

                                </Grid>
                            );
                        })}
                    </Grid>
                </Stack>
            )}
            {albums?.length === 0 && (
                <Box m={'auto'}>
                    <NotFoundContent
                        name={'albums'}
                    />
                </Box>
            )}
        </>

    );
};

export default Albums;