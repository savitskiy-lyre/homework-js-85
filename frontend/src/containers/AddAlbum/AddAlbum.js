import React, {useEffect, useState} from 'react';
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import ForumIcon from "@mui/icons-material/Forum";
import Typography from "@mui/material/Typography";
import {Stack} from "@mui/material";
import FormElement from "../../components/UI/Form/FormElement";
import TextField from "@mui/material/TextField";
import LoadingButton from "@mui/lab/LoadingButton";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/music/noAuth/artistsActions";
import {addAlbum, addAlbumFailure} from "../../store/actions/music/noAuth/albumsActions";

const AddAlbum = () => {
    const dispatch = useDispatch();
    const formErr = useSelector(state => state.albums.addFormErr);
    const loading = useSelector(state => state.albums.addFormBtnLoading);
    const artists = useSelector(state => state.artists.artists);

    const [state, setState] = useState({
        title: '',
        date: '',
        author: '',
        image: undefined,
    });


    const getErrorMessage = field => {
        return formErr?.errors[field]?.message;
    }

    const formSubmit = (e) => {
        e.preventDefault();
        const newItem = new FormData();
        Object.keys(state).forEach(key => {
            newItem.append(key, state[key]);
        })
        dispatch(addAlbum(newItem));
    };
    const inputChanger = (e) => {
        if (!state.title) dispatch(addAlbumFailure(null));
        if (!state.date) dispatch(addAlbumFailure(null));
        if (!state.author) dispatch(addAlbumFailure(null));
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };
    const inputImgChanger = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.files[0]}));
    }
    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    return (
        <Box
            mt={6}
            textAlign={'center'}
        >
            <Box
                sx={{
                    display: 'inline-flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                width={'100%'}
                maxWidth={'500px'}
            >
                <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                    <ForumIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add new album
                </Typography>
                <Stack
                    component="form"
                    noValidate
                    spacing={4}
                    sx={{mt: 1}}
                    width={'100%'}
                    onSubmit={formSubmit}
                >
                    <FormElement
                        label={'Title'}
                        name={'title'}
                        onChange={inputChanger}
                        value={state.title}
                        error={getErrorMessage('title')}
                        required
                    />
                    <FormElement
                        label={'Date'}
                        name={'date'}
                        onChange={inputChanger}
                        value={state.date}
                        error={getErrorMessage('date')}
                        required
                    />
                    <FormElement
                        select
                        label={'Author'}
                        name={'author'}
                        onChange={inputChanger}
                        value={state.author}
                        error={getErrorMessage('author') ? 'Choose Author pls !!!' : ''}
                        options={{
                            data: artists,
                            key: 'name',
                        }}
                        required
                    />
                    <TextField
                        type={"file"}
                        name="image"
                        onChange={inputImgChanger}
                    />
                    <LoadingButton
                        type="submit"
                        loading={loading}
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Submit
                    </LoadingButton>
                </Stack>
            </Box>
        </Box>
    );
};

export default AddAlbum;