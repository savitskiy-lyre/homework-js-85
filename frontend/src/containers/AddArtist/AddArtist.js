import React, {useState} from 'react';
import Box from "@mui/material/Box";
import Avatar from "@mui/material/Avatar";
import ForumIcon from "@mui/icons-material/Forum";
import Typography from "@mui/material/Typography";
import {Stack} from "@mui/material";
import TextField from "@mui/material/TextField";
import LoadingButton from "@mui/lab/LoadingButton";
import FormElement from "../../components/UI/Form/FormElement";
import {addArtist, addArtistFailure} from "../../store/actions/music/noAuth/artistsActions";
import {useDispatch, useSelector} from "react-redux";

const AddArtist = () => {
    const dispatch = useDispatch();
    const formErr = useSelector(state => state.artists.addArtistErr);
    const loading = useSelector(state => state.artists.addArtistLoading);
    const getErrorMessage = field => {
        return formErr?.errors[field]?.message;
    }
    const [state, setState] = useState({
        name: '',
        description: '',
        photo: undefined,
    });

    const artistSubmit = (e) => {
        e.preventDefault();
        const newArtist = new FormData();
        Object.keys(state).forEach(key => {
            newArtist.append(key, state[key]);
        })
        dispatch(addArtist(newArtist));
    };

    const inputChanger = (e) => {
        if (!state.name) dispatch(addArtistFailure(null));
        setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
    };
    const inputImgChanger = (e) => {
        setState(prevState => ({...prevState, [e.target.name]: e.target.files[0]}));
    }

    return (
        <Box
            mt={6}
            textAlign={'center'}
        >
            <Box
                sx={{
                    display: 'inline-flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
                width={'100%'}
                maxWidth={'500px'}
            >
                <Avatar sx={{m: 1, bgcolor: 'primary.main'}}>
                    <ForumIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Add new artist
                </Typography>
                <Stack
                    component="form"
                    noValidate
                    spacing={4}
                    sx={{mt: 1}}
                    width={'100%'}
                    onSubmit={artistSubmit}
                >
                    <FormElement
                        label={'Name'}
                        name={'name'}
                        onChange={inputChanger}
                        value={state.name}
                        error={getErrorMessage('name')}
                        required
                    />
                    <FormElement
                        label={'Description'}
                        name={'description'}
                        onChange={inputChanger}
                        value={state.description}
                    />
                    <TextField
                        type={"file"}
                        name="photo"
                        onChange={inputImgChanger}
                    />
                    <LoadingButton
                        type="submit"
                        loading={loading}
                        variant="contained"
                        sx={{mt: 3, mb: 2}}
                    >
                        Submit
                    </LoadingButton>
                </Stack>
            </Box>
        </Box>
    );
};

export default AddArtist;