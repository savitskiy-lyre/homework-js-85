import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import NotFoundContent from "../../components/UI/NotFoundContent/NotFoundContent";
import Box from "@mui/material/Box";
import TracksSkeleton from "../../components/UI/TracksSkeleton";
import {fetchTrackHistory} from "../../store/actions/music/auth/trackHistoryActions";
import {Redirect} from "react-router-dom";

const TrackHistory = () => {
    const trackHistory = useSelector(state => state.trackHistory.trackHistory);
    const profile = useSelector(state => state.profile.profile);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchTrackHistory());
    }, [dispatch])
    if (!profile) {
        return (<Redirect to={'/signin'}/>)
    }
    return (
        <>
            {trackHistory?.length > 0 && (
                <TableContainer component={Paper} sx={{marginTop: '20px', height: 'auto'}}>
                    <Table sx={{minWidth: '250px'}} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">track</TableCell>
                                <TableCell align="right">username</TableCell>
                                <TableCell align="right">datetime</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {trackHistory.map((data) => (
                                <TableRow
                                    key={data._id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell align="left">{data.track.title}</TableCell>
                                    <TableCell align="right">{data.user.username}</TableCell>
                                    <TableCell align="right">{data.datetime}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            )}
            {trackHistory?.length === 0 && (
                <Box m={'auto'}>
                    <NotFoundContent
                        name={"track's history"}
                    />
                </Box>
            )
            }
            {!trackHistory && (
                <Stack mt={5}>
                    <TracksSkeleton/>
                    <TracksSkeleton/>
                    <TracksSkeleton/>
                </Stack>
            )}
        </>
    );
};

export default TrackHistory;