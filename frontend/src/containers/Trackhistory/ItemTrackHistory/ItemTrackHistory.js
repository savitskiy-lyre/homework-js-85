import React from 'react';
import {Grid, Paper, Typography} from "@mui/material";

const ItemTrackHistory = ({datetime,authorName,trackTitle}) => {
    return (
        <Paper>
            <Grid container justifyContent={"space-between"} spacing={2} p={1}>
                <Grid item>
                    <Typography fontSize={19} component={"strong"}>
                        <i>Author: </i>{authorName}
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography>
                        {trackTitle}
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography>
                        {datetime}
                    </Typography>
                </Grid>
            </Grid>

        </Paper>
    );
};

export default ItemTrackHistory;