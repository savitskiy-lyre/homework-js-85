import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, Grid, Stack, Typography} from "@mui/material";
import Artist from "../components/Artist";
import {ARTISTS_URL, IMAGES_URL} from "../config";
import notFoundImg from "../assets/image/notFound.png";
import ArtistsSkeleton from "../components/UI/ArtistsSkeleton";
import NotFoundContent from "../components/UI/NotFoundContent/NotFoundContent";
import {fetchArtists} from "../store/actions/music/noAuth/artistsActions";
import ItemContextMenu from "../components/UI/ItemContextMenu/ItemContextMenu";
import PublishBackdrop from "../components/UI/PublishBackdrop/PublishBackdrop";
import {Link as RouterLink} from "react-router-dom";
import {deleteItem, publishItem} from "../store/actions/adminActions";

const Artists = () => {
    const artists = useSelector(state => state.artists.artists);
    const profile = useSelector(state => state.profile.profile);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);
    return (
        <>
            {profile && (
                <Button
                    component={RouterLink}
                    size="large"
                    to={'artists/add'}
                >
                    Add
                </Button>
            )}
            {!artists && (
                <Grid container spacing={4} my={5} direction={"column"}>
                    <ArtistsSkeleton/>
                    <ArtistsSkeleton/>
                    <ArtistsSkeleton/>
                </Grid>
            )}
            {artists?.length > 0 && (
                <Stack spacing={2} mt={3}>
                    <Typography variant={'h4'} component={'h1'}>
                        List of artists:
                    </Typography>
                    <Grid container spacing={2} justifyContent={"center"}>
                        {artists.map((artist) => {
                            if (!artist.published && profile?.role !== 'admin') return null;
                            return (
                                <Grid item key={artist._id}>
                                    <ItemContextMenu
                                        menuItems={profile?.role === 'admin' ? [
                                            {
                                                _id: artist._id + 'publishMenu',
                                                title: artist.published ? 'Unpublish' : 'Publish',
                                                menuHandler: async () => {
                                                    await dispatch(publishItem(ARTISTS_URL + `/${artist._id}/publish`, !artist.published));
                                                    dispatch(fetchArtists());
                                                },
                                            }, {
                                                _id: artist._id + 'deleteMenu',
                                                title: 'Delete',
                                                menuHandler: async () => {
                                                    await dispatch(deleteItem(ARTISTS_URL + `/` + artist._id));
                                                    dispatch(fetchArtists());
                                                },
                                            },
                                        ] : null}
                                    >
                                        <>
                                            <Artist
                                                name={artist.name}
                                                photo={artist.photo ? IMAGES_URL + artist.photo : notFoundImg}
                                                _id={artist._id}
                                            >
                                                {!artist.published && (
                                                    <PublishBackdrop/>
                                                )}
                                            </Artist>
                                        </>
                                    </ItemContextMenu>
                                </Grid>

                            )
                                ;
                        })}
                    </Grid>
                </Stack>
            )}
            {artists?.length === 0 && (
                <Box m={"auto"}>
                    <NotFoundContent
                        name={'tracks'}
                    />
                </Box>
            )

            }
        </>
    );
};

export default Artists;