export const saveToLocalStorage = state => {
    try {
        localStorage.setItem('musicProfileState', JSON.stringify(state));
    } catch (e) {
        console.log('Could not save state');
    }
};

export const loadFromLocalStorage = () => {
    try {
        const loadedState = localStorage.getItem('musicProfileState');

        if (loadedState === null) {
            return undefined;
        }

        return JSON.parse(loadedState);
    } catch (err) {
        return undefined;
    }
};