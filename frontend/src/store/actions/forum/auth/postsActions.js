import {axiosApi} from "../../../../axiosApi";
import {POSTS_URL} from "../../../../config";
import {historyReplace} from "../../historyActions";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';
export const ADD_POST_REQUEST = 'ADD_POST_REQUEST';
export const ADD_POST_SUCCESS = 'ADD_POST_SUCCESS';
export const ADD_POST_FAILURE = 'ADD_POST_FAILURE';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = (data) => ({type: FETCH_POSTS_SUCCESS, payload: data});
export const fetchPostsFailure = (error) => ({type: FETCH_POSTS_FAILURE, payload: error});

export const fetchPosts = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchPostsRequest());
            const {data} = await axiosApi.get(POSTS_URL);
            dispatch(fetchPostsSuccess(data));
        } catch (error) {
            dispatch(fetchPostsFailure(error?.response?.data || {error: error.message}));
        }
    }
}

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = (data) => ({type: FETCH_POST_SUCCESS, payload: data});
export const fetchPostFailure = (error) => ({type: FETCH_POST_FAILURE, payload: error});

export const fetchPost = (id) => {
    return async (dispatch) => {
        try {
            dispatch(fetchPostRequest());
            const {data} = await axiosApi.get(POSTS_URL + `/${id}`);
            dispatch(fetchPostSuccess(data));
        } catch (error) {
            dispatch(fetchPostFailure(error?.response?.data || {error: error.message}));
        }
    }
}
export const addPostRequest = () => ({type: ADD_POST_REQUEST});
export const addPostSuccess = (data) => ({type: ADD_POST_SUCCESS, payload: data});
export const addPostFailure = (error) => ({type: ADD_POST_FAILURE, payload: error});

export const addPost = (newPost) => {
    return async (dispatch, getState) => {
        try {
            const {profile} = getState()
            dispatch(addPostRequest());
            const {data} = await axiosApi.post(
                POSTS_URL,
                newPost,
                {
                    headers: {
                        Authorization: profile.profile.token,
                    }
                });
            dispatch(addPostSuccess(data));
            dispatch(historyReplace('/posts'));
        } catch (error) {
            dispatch(addPostFailure(error?.response?.data || {error: error.message}));
        }
    }
}