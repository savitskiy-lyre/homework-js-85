import {axiosApi} from "../../../../axiosApi";
import {COMMENTS_URL} from "../../../../config";

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';
export const ADD_COMMENT_REQUEST = 'ADD_COMMENT_REQUEST';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = (data) => ({type: FETCH_COMMENTS_SUCCESS, payload: data});
export const fetchCommentsFailure = (error) => ({type: FETCH_COMMENTS_FAILURE, payload: error});

export const fetchComments = (postId) => {
    return async (dispatch) => {
        try {
            dispatch(fetchCommentsRequest());
            const {data} = await axiosApi.get(COMMENTS_URL + (postId ? `?post=${postId}` : ''));
            dispatch(fetchCommentsSuccess(data));
        } catch (error) {
            dispatch(fetchCommentsFailure(error?.response?.data || {error: error.message}));
        }
    }
}

export const addCommentRequest = () => ({type: ADD_COMMENT_REQUEST});
export const addCommentSuccess = (data) => ({type: ADD_COMMENT_SUCCESS, payload: data});
export const addCommentFailure = (error) => ({type: ADD_COMMENT_FAILURE, payload: error});

export const addComment = (newComment) => {
    return async (dispatch, getState) => {
        const {profile} = getState();
        try {
            dispatch(addCommentRequest());
            const {data} = await axiosApi.post(COMMENTS_URL, newComment, {
                headers: {
                    Authorization: profile.profile.token
                }
            });
            dispatch(addCommentSuccess(data));
        } catch (error) {
            dispatch(addCommentFailure(error?.response?.data || {error: error.message}));
        }
    }
}