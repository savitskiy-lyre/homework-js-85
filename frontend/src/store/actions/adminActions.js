import {axiosApi} from "../../axiosApi";
import {toast} from "react-toastify";

export const PUBLISH_ITEM_REQUEST = 'PUBLISH_ITEM_REQUEST';
export const PUBLISH_ITEM_SUCCESS = 'PUBLISH_ITEM_SUCCESS';
export const PUBLISH_ITEM_FAILURE = 'PUBLISH_ITEM_FAILURE';

export const publishItemRequest = () => ({type: PUBLISH_ITEM_REQUEST});
export const publishItemSuccess = () => ({type: PUBLISH_ITEM_SUCCESS});
export const publishItemFailure = (error) => ({type: PUBLISH_ITEM_FAILURE, payload: error});

export const publishItem = (url, bool) => {
    return async (dispatch, getState) => {
        try {
            const profile = getState().profile?.profile;
            let headers;
            if (profile) {
                headers = {
                    Authorization: profile.token,
                }
            }
            dispatch(publishItemRequest());
            await axiosApi.put(url, {published: bool}, {headers});
            dispatch(publishItemSuccess());
            toast.success(bool ? 'Published' : 'Unpublish');
        } catch (error) {
            const errorData = error?.response?.data;
            dispatch(publishItemFailure(errorData));
            toast.error(errorData?.error || ("Can't be" + bool ? 'publish' : 'unpublish'));
        }
    }
}

export const DELETE_ITEM_REQUEST = 'DELETE_ITEM_REQUEST';
export const DELETE_ITEM_SUCCESS = 'DELETE_ITEM_SUCCESS';
export const DELETE_ITEM_FAILURE = 'DELETE_ITEM_FAILURE';

export const deleteItemRequest = () => ({type: DELETE_ITEM_REQUEST});
export const deleteItemSuccess = () => ({type: DELETE_ITEM_SUCCESS});
export const deleteItemFailure = (error) => ({type: DELETE_ITEM_FAILURE, payload: error});

export const deleteItem = (url) => {
    return async (dispatch, getState) => {
        try {
            const profile = getState().profile?.profile;
            let headers;
            if (profile) {
                headers = {
                    Authorization: profile.token,
                }
            }
            dispatch(deleteItemRequest());
            await axiosApi.delete(url, {headers});
            dispatch(deleteItemSuccess());
            toast.success('The deletion was successful');
        } catch (error) {
            const errorData = error?.response?.data;
            dispatch(deleteItemFailure(errorData));
            toast.error(errorData?.error || "The deletion was unsuccessful");
        }
    }
}