import {axiosApi} from "../../../../axiosApi";
import {USER_FC_LOGIN_URL, USERS_SESSIONS_URL, USERS_URL} from "../../../../config";
import {historyPush, historyReplace} from "../../historyActions";
import {toast} from "react-toastify";

export const CREATE_ACCOUNT_REQUEST = 'CREATE_ACCOUNT_REQUEST';
export const CREATE_ACCOUNT_SUCCESS = 'CREATE_ACCOUNT_SUCCESS';
export const CREATE_ACCOUNT_FAILURE = 'CREATE_ACCOUNT_FAILURE';
export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';

export const USER_LOGOUT = 'USER_LOGOUT';

export const createAccountRequest = () => ({type: CREATE_ACCOUNT_REQUEST});
export const createAccountSuccess = (data) => ({type: CREATE_ACCOUNT_SUCCESS, payload: data});
export const createAccountFailure = (error) => ({type: CREATE_ACCOUNT_FAILURE, payload: error});

export const createAccount = (account) => {
    return async (dispatch) => {
        try {
            dispatch(createAccountRequest());
            const {data} = await axiosApi.post(USERS_URL, account);
            dispatch(createAccountSuccess(data));
            dispatch(historyReplace('/'));
        } catch (error) {
            dispatch(createAccountFailure(error?.response?.data || {error: error.message}));
        }
    }
}
export const signInRequest = () => ({type: SIGN_IN_REQUEST});
export const signInSuccess = (data) => ({type: SIGN_IN_SUCCESS, payload: data});
export const signInFailure = (error) => ({type: SIGN_IN_FAILURE, payload: error});

export const signIn = (account) => {
    return async (dispatch) => {
        try {
            dispatch(signInRequest());
            const {data} = await axiosApi.post(USERS_SESSIONS_URL, account);
            dispatch(signInSuccess(data));
            dispatch(historyPush('/'));
            toast.success('Login successful');
        } catch (err) {
            toast.error(err.response.data.global);
            dispatch(signInFailure(err?.response?.data || {error: err.message}));
        }
    }
}

export const facebookLogin = userData => {
    return async dispatch => {
        try {
            dispatch(signInRequest());
            const {data} = await axiosApi.post(USER_FC_LOGIN_URL, userData);
            dispatch(signInSuccess(data));
            dispatch(historyPush('/'));
            toast.success('Login successful');
        } catch (err) {
            toast.error(err.response.data.global);
            dispatch(signInFailure({error: err.response.data.global}));
        }
    };
};

export const userLogout = () => {
    return async (dispatch, getState) => {
        try {
            const {profile} = getState();
            await axiosApi.delete(USERS_SESSIONS_URL, {
                headers: {
                    Authorization: profile.profile.token,
                },
            });
            dispatch({type: USER_LOGOUT});
            dispatch(historyPush('/'));
        } catch (err) {
            console.log(err);
        }
    };
};