import {axiosApi} from "../../../../axiosApi";
import {TRACK_HISTORY_URL} from "../../../../config";

export const FETCH_TRACK_HISTORY_REQUEST = 'FETCH_TRACK_HISTORY_REQUEST';
export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const FETCH_TRACK_HISTORY_FAILURE = 'FETCH_TRACK_HISTORY_FAILURE';
export const ADD_TRACK_TO_HISTORY_REQUEST = 'ADD_TRACK_TO_HISTORY_REQUEST';
export const ADD_TRACK_TO_HISTORY_SUCCESS = 'ADD_TRACK_TO_HISTORY_SUCCESS';
export const ADD_TRACK_TO_HISTORY_FAILURE = 'ADD_TRACK_TO_HISTORY_FAILURE';

export const addTrackToHistoryRequest = () => ({type: ADD_TRACK_TO_HISTORY_REQUEST});
export const addTrackToHistorySuccess = (data) => ({type: ADD_TRACK_TO_HISTORY_SUCCESS, payload: data});
export const addTrackToHistoryFailure = (error) => ({type: ADD_TRACK_TO_HISTORY_FAILURE, payload: error});

export const addTrackToHistory = (id) => {
    return async (dispatch, getState) => {
        try {
            const {profile} = getState();
            dispatch(addTrackToHistoryRequest());
            const {data} = await axiosApi.post(
                TRACK_HISTORY_URL,
                {
                    track_id: id,
                    datetime: new Date().toISOString(),
                },
                {
                    headers: {
                        Authorization: profile?.profile?.token,
                    },
                });
            dispatch(addTrackToHistorySuccess(data));
        } catch (error) {
            dispatch(addTrackToHistoryFailure(error));
        }
    }
}
export const fetchTrackHistoryRequest = () => ({type: FETCH_TRACK_HISTORY_REQUEST});
export const fetchTrackHistorySuccess = (data) => ({type: FETCH_TRACK_HISTORY_SUCCESS, payload: data});
export const fetchTrackHistoryFailure = (error) => ({type: FETCH_TRACK_HISTORY_FAILURE, payload: error});

export const fetchTrackHistory = () => {
    return async (dispatch, getState) => {
        try {
            const {profile} = getState();
            dispatch(fetchTrackHistoryRequest());
            const {data} = await axiosApi.get(TRACK_HISTORY_URL, {
                headers: {
                    Authorization: profile?.profile?.token,
                },
            });

            dispatch(fetchTrackHistorySuccess(data));
        } catch (error) {
            dispatch(fetchTrackHistoryFailure(error));
        }
    }
}