import {axiosApi} from "../../../../axiosApi";
import {TRACKS_URL} from "../../../../config";
import {historyBack} from "../../historyActions";
import {toast} from "react-toastify";

export const FETCH_ALBUM_TRACKS_REQUEST = 'FETCH_ALBUM_TRACKS_REQUEST';
export const FETCH_ALBUM_TRACKS_SUCCESS = 'FETCH_ALBUM_TRACKS_SUCCESS';
export const FETCH_ALBUM_TRACKS_FAILURE = 'FETCH_ALBUM_TRACKS_FAILURE';

export const fetchAlbumTracksRequest = () => ({type: FETCH_ALBUM_TRACKS_REQUEST});
export const fetchAlbumTracksSuccess = (data) => ({type: FETCH_ALBUM_TRACKS_SUCCESS, payload: data});
export const fetchAlbumTracksFailure = (error) => ({type: FETCH_ALBUM_TRACKS_FAILURE, payload: error});

export const fetchAlbumTracks = (search) => {
    return async (dispatch) => {
        try {
            dispatch(fetchAlbumTracksRequest());
            const {data} = await axiosApi.get(TRACKS_URL + search);
            dispatch(fetchAlbumTracksSuccess(data));
        } catch (error) {
            dispatch(fetchAlbumTracksFailure(error));
        }
    }
}
export const ADD_TRACK_REQUEST = 'ADD_TRACK_REQUEST';
export const ADD_TRACK_SUCCESS = 'ADD_TRACK_SUCCESS';
export const ADD_TRACK_FAILURE = 'ADD_TRACK_FAILURE';

export const addTrackRequest = () => ({type: ADD_TRACK_REQUEST});
export const addTrackSuccess = () => ({type: ADD_TRACK_SUCCESS});
export const addTrackFailure = (error) => ({type: ADD_TRACK_FAILURE, payload: error});

export const addTrack = (item) => {
    return async (dispatch, getState) => {
        try {
            const profile = getState().profile?.profile;
            let headers;
            if (profile) {
                headers = {
                    Authorization: profile.token,
                }
            }
            dispatch(addTrackRequest());
            await axiosApi.post(TRACKS_URL, item, {headers});
            dispatch(addTrackSuccess());
            dispatch(historyBack());
            toast.success('Track created');
        } catch (error) {
            const errorData = error?.response?.data;
            dispatch(addTrackFailure(errorData));
            toast.error(errorData?.error || 'The track cannot be created');
        }
    }
}