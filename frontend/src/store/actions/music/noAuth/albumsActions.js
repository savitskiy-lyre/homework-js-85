import {axiosApi} from "../../../../axiosApi";
import {ALBUMS_URL} from "../../../../config";
import {historyBack} from "../../historyActions";
import {toast} from "react-toastify";

export const FETCH_ARTIST_ALBUMS_REQUEST = 'FETCH_ARTIST_ALBUMS_REQUEST';
export const FETCH_ARTIST_ALBUMS_SUCCESS = 'FETCH_ARTIST_ALBUMS_SUCCESS';
export const FETCH_ARTIST_ALBUMS_FAILURE = 'FETCH_ARTIST_ALBUMS_FAILURE';

export const fetchArtistAlbumsRequest = () => ({type: FETCH_ARTIST_ALBUMS_REQUEST});
export const fetchArtistAlbumsSuccess = (data) => ({type: FETCH_ARTIST_ALBUMS_SUCCESS, payload: data});
export const fetchArtistAlbumsFailure = (error) => ({type: FETCH_ARTIST_ALBUMS_FAILURE, payload: error});

export const fetchArtistAlbums = (search) => {
    return async (dispatch) => {
        try {
            dispatch(fetchArtistAlbumsRequest());
            const {data} = await axiosApi.get(ALBUMS_URL + search);
            dispatch(fetchArtistAlbumsSuccess(data));
        } catch (error) {
            dispatch(fetchArtistAlbumsFailure(error));
        }
    }
}
export const ADD_ALBUM_REQUEST = 'ADD_ALBUM_REQUEST';
export const ADD_ALBUM_SUCCESS = 'ADD_ALBUM_SUCCESS';
export const ADD_ALBUM_FAILURE = 'ADD_ALBUM_FAILURE';

export const addAlbumRequest = () => ({type: ADD_ALBUM_REQUEST});
export const addAlbumSuccess = () => ({type: ADD_ALBUM_SUCCESS});
export const addAlbumFailure = (error) => ({type: ADD_ALBUM_FAILURE, payload: error});

export const addAlbum = (newArtist) => {
    return async (dispatch, getState) => {
        try {
            const profile = getState().profile?.profile;
            let headers;
            if (profile) {
                headers = {
                    Authorization: profile.token,
                }
            }
            dispatch(addAlbumRequest());
            await axiosApi.post(ALBUMS_URL, newArtist, {headers});
            dispatch(addAlbumSuccess());
            dispatch(historyBack());
            toast.success('Album created');
        } catch (error) {
            const errorData = error?.response?.data;
            dispatch(addAlbumFailure(errorData));
            toast.error(errorData?.error || 'The album cannot be created');
        }
    }
}