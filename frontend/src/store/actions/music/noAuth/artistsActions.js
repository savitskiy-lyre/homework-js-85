import {axiosApi} from "../../../../axiosApi";
import {ARTISTS_URL} from "../../../../config";
import {toast} from "react-toastify";
import {historyBack} from "../../historyActions";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = (data) => ({type: FETCH_ARTISTS_SUCCESS, payload: data});
export const fetchArtistsFailure = (error) => ({type: FETCH_ARTISTS_FAILURE, payload: error});

export const fetchArtists = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchArtistsRequest());
            const {data} = await axiosApi.get(ARTISTS_URL);
            dispatch(fetchArtistsSuccess(data));
        } catch (error) {
            dispatch(fetchArtistsFailure(error));
        }
    }
}

export const ADD_ARTIST_REQUEST = 'ADD_ARTIST_REQUEST';
export const ADD_ARTIST_SUCCESS = 'ADD_ARTIST_SUCCESS';
export const ADD_ARTIST_FAILURE = 'ADD_ARTIST_FAILURE';

export const addArtistRequest = () => ({type: ADD_ARTIST_REQUEST});
export const addArtistSuccess = () => ({type: ADD_ARTIST_SUCCESS});
export const addArtistFailure = (error) => ({type: ADD_ARTIST_FAILURE, payload: error});

export const addArtist = (item) => {
    return async (dispatch, getState) => {
        try {
            const profile = getState().profile?.profile;
            let headers;
            if (profile) {
                headers = {
                    Authorization: profile.token,
                }
            }
            dispatch(addArtistRequest());
            await axiosApi.post(ARTISTS_URL, item, {headers});
            dispatch(addArtistSuccess());
            dispatch(historyBack());
            toast.success('Artist created');
        } catch (error) {
            const errorData = error?.response?.data;
            dispatch(addArtistFailure(errorData));
            toast.error(errorData?.error || 'The artist cannot be created');
        }
    }
}