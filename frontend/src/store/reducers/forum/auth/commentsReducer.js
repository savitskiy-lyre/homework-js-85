import {
    ADD_COMMENT_FAILURE,
    ADD_COMMENT_REQUEST, ADD_COMMENT_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS
} from "../../../actions/forum/auth/commentsActions";


const initState = {
    data: null,
    fetchErr: null,
    submitBtnLoading: false,
};

export const commentsReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_COMMENTS_SUCCESS:
            return {...state, data: action.payload}
        case FETCH_COMMENTS_FAILURE:
            return {...state, fetchErr: action.payload}
        case ADD_COMMENT_REQUEST:
            return {...state, fetchErr: null , submitBtnLoading: true}
        case ADD_COMMENT_SUCCESS:
            return {...state, data: [...state.data, action.payload], submitBtnLoading: false}
        case ADD_COMMENT_FAILURE:
            return {...state, fetchErr: action.payload, submitBtnLoading: false}
        default:
            return state;
    }
}