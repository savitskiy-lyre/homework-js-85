import {
    ADD_POST_FAILURE,
    ADD_POST_REQUEST, ADD_POST_SUCCESS, FETCH_POST_FAILURE, FETCH_POST_REQUEST, FETCH_POST_SUCCESS,
    FETCH_POSTS_FAILURE,
    FETCH_POSTS_REQUEST,
    FETCH_POSTS_SUCCESS
} from "../../../actions/forum/auth/postsActions";

const initState = {
    data: null,
    currentPost: null,
    fetchErr: null,
    submitBtnLoading: false,
};

export const postsReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_POSTS_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_POSTS_SUCCESS:
            return {...state, data: action.payload}
        case FETCH_POSTS_FAILURE:
            return {...state, fetchErr: action.payload}
        case FETCH_POST_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_POST_SUCCESS:
            return {...state, currentPost: action.payload}
        case FETCH_POST_FAILURE:
            return {...state, fetchErr: action.payload}
        case ADD_POST_REQUEST:
            return {...state, fetchErr: null, submitBtnLoading: true}
        case ADD_POST_SUCCESS:
            return {...state, data: [action.payload,...state.data], submitBtnLoading: false}
        case ADD_POST_FAILURE:
            return {...state, fetchErr: action.payload, submitBtnLoading: false}

        default:
            return state;
    }
}