import {PUBLISH_ITEM_FAILURE, PUBLISH_ITEM_REQUEST, PUBLISH_ITEM_SUCCESS} from "../actions/adminActions";

const initState = {
    publishErr: null,
};
export const adminReducer = (state = initState, action) => {
    switch (action.type) {
        case PUBLISH_ITEM_REQUEST:
            return {...state, publishErr: null}
        case PUBLISH_ITEM_SUCCESS:
            return {...state}
        case PUBLISH_ITEM_FAILURE:
            return {...state, publishErr: action.payload}
        default:
            return state;
    }
}