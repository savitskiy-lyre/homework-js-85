import {
    ADD_TRACK_TO_HISTORY_FAILURE,
    ADD_TRACK_TO_HISTORY_REQUEST,
    ADD_TRACK_TO_HISTORY_SUCCESS,
    FETCH_TRACK_HISTORY_FAILURE,
    FETCH_TRACK_HISTORY_REQUEST,
    FETCH_TRACK_HISTORY_SUCCESS
} from "../../../actions/music/auth/trackHistoryActions";

const initState = {
    trackHistory: null,
    addErr: null,
    fetchErr: null,
};
export const trackHistoryReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_TRACK_TO_HISTORY_REQUEST:
            return {...state, addErr: null}
        case ADD_TRACK_TO_HISTORY_SUCCESS:
            return {...state, trackHistory: [...state.tracksHistories, action.payload]}
        case ADD_TRACK_TO_HISTORY_FAILURE:
            return {...state, addErr: action.payload}
        case FETCH_TRACK_HISTORY_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, trackHistory: action.payload}
        case FETCH_TRACK_HISTORY_FAILURE:
            return {...state, fetchErr: action.payload}

        default:
            return state;
    }
}