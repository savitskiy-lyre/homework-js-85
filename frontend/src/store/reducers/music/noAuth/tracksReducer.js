import {
    ADD_TRACK_FAILURE,
    ADD_TRACK_REQUEST,
    ADD_TRACK_SUCCESS,
    FETCH_ALBUM_TRACKS_FAILURE,
    FETCH_ALBUM_TRACKS_REQUEST,
    FETCH_ALBUM_TRACKS_SUCCESS
} from "../../../actions/music/noAuth/tracksActions";

const initState = {
    tracks: null,
    fetchErr: null,
    addFormErr: null,
    addFormBtnLoading: null,
};
export const tracksReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_ALBUM_TRACKS_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_ALBUM_TRACKS_SUCCESS:
            return {...state, tracks: action.payload}
        case FETCH_ALBUM_TRACKS_FAILURE:
            return {...state, fetchErr: action.payload}
        case ADD_TRACK_REQUEST:
            return {...state, addFormErr: null, addFormBtnLoading: true}
        case ADD_TRACK_SUCCESS:
            return {...state, addFormBtnLoading: false}
        case ADD_TRACK_FAILURE:
            return {...state, addFormErr: action.payload, addFormBtnLoading: false}
        default:
            return state;
    }
}