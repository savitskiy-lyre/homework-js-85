import {
    ADD_ALBUM_FAILURE,
    ADD_ALBUM_REQUEST,
    ADD_ALBUM_SUCCESS,
    FETCH_ARTIST_ALBUMS_FAILURE,
    FETCH_ARTIST_ALBUMS_REQUEST,
    FETCH_ARTIST_ALBUMS_SUCCESS
} from "../../../actions/music/noAuth/albumsActions";

const initState = {
    albums: null,
    fetchErr: null,
    addFormErr: null,
    addFormBtnLoading: null,
};
export const albumsReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_ARTIST_ALBUMS_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_ARTIST_ALBUMS_SUCCESS:
            return {...state, albums: action.payload}
        case FETCH_ARTIST_ALBUMS_FAILURE:
            return {...state, fetchErr: action.payload}
        case ADD_ALBUM_REQUEST:
            return {...state, addFormErr: null, addFormBtnLoading: true}
        case ADD_ALBUM_SUCCESS:
            return {...state, addFormBtnLoading: false}
        case ADD_ALBUM_FAILURE:
            return {...state, addFormErr: action.payload, addFormBtnLoading: false}

        default:
            return state;
    }
}