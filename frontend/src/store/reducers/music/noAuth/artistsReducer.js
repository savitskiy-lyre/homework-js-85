import {
    ADD_ARTIST_FAILURE,
    ADD_ARTIST_REQUEST,
    ADD_ARTIST_SUCCESS,
    FETCH_ARTISTS_FAILURE,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS
} from "../../../actions/music/noAuth/artistsActions";

const initState = {
    artists: null,
    fetchErr: null,
    addArtistErr: null,
    addArtistLoading: false,
};
export const artistsReducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_REQUEST:
            return {...state, fetchErr: null}
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.payload}
        case FETCH_ARTISTS_FAILURE:
            return {...state, fetchErr: action.payload}
        case ADD_ARTIST_REQUEST:
            return {...state, addArtistErr: null, addArtistLoading: true}
        case ADD_ARTIST_SUCCESS:
            return {...state, addArtistLoading: false}
        case ADD_ARTIST_FAILURE:
            return {...state, addArtistErr: action.payload, addArtistLoading: false}
        default:
            return state;
    }
}