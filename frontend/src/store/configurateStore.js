import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {initState, profileReducer} from "./reducers/music/auth/profileReducer";
import {trackHistoryReducer} from "./reducers/music/auth/trackHistoryReducer";
import {artistsReducer} from "./reducers/music/noAuth/artistsReducer";
import {albumsReducer} from "./reducers/music/noAuth/albumsReducer";
import {tracksReducer} from "./reducers/music/noAuth/tracksReducer";
import {postsReducer} from "./reducers/forum/auth/postsReducer";
import thunk from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import {commentsReducer} from "./reducers/forum/auth/commentsReducer";
import {adminReducer} from "./reducers/adminReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    profile: profileReducer,
    trackHistory: trackHistoryReducer,
    artists: artistsReducer,
    albums: albumsReducer,
    tracks: tracksReducer,
    posts: postsReducer,
    comments: commentsReducer,
    admin: adminReducer,
});
const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk),
    ));

store.subscribe(() => {
    saveToLocalStorage({
        profile: {
            ...initState,
            profile: store.getState().profile.profile,
        },
    });
})

export default store;