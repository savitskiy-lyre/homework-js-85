const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TrackHistorySchema = new mongoose.Schema({
   user: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'User ID is required'],
      ref: 'users',
   },
   track: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'Track ID is required'],
      ref: 'tracks',
   },
   datetime: {
      type: String,
      default: new Date().toISOString(),
      required: [true, 'Datetime is required string'],
   }
})

TrackHistorySchema.plugin(idValidator);
const TrackHistory = mongoose.model("track's_histories", TrackHistorySchema);
module.exports = TrackHistory;