const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const CommentSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'User ID is required'],
        ref: 'users',
    },
    post: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Post ID is required'],
        ref: 'posts',
    },
    text: {
        type: String,
        required: [true, 'Text is required'],
    },
})

CommentSchema.plugin(idValidator);
const Comment = mongoose.model('comments', CommentSchema);

module.exports = Comment;