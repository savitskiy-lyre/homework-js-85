const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const AlbumSchema = new mongoose.Schema({
   title: {
      type: String,
      required: [true, 'Title is required'],
   },
   published: {
      type: Boolean,
      default: false,
   },
   author: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'Author ID is required'],
      ref: 'artists',
   },
   date: {
      type: String,
      required: [true, 'Date is required'],
   },
   image: String,
})

AlbumSchema.plugin(idValidator);
const Album = mongoose.model('albums', AlbumSchema);
module.exports = Album;