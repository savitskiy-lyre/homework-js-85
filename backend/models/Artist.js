const mongoose = require('mongoose');
const ArtistSchema = new mongoose.Schema({
   name: {
      type: String,
      required: [true, 'Name is required'],
   },
   published: {
      type: Boolean,
      default: false,
   },
   photo: String,
   description: String,
})

const Artist = mongoose.model('artists', ArtistSchema);

module.exports = Artist;