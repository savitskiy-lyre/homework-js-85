const mongoose = require('mongoose');
const moment = require('moment');
const idValidator = require('mongoose-id-validator');

const PostSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Title is required'],
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'User ID is required'],
        ref: 'users',
    },
    description: String,
    image: String,
    datetime: {
        type:  String,
        default: moment().format('LLL'),
    }
})

PostSchema.plugin(idValidator);
const Post = mongoose.model('posts', PostSchema);

module.exports = Post;