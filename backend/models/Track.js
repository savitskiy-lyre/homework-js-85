const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TrackSchema = new mongoose.Schema({
   title: {
      type: String,
      required: [true, 'Title is required'],
   },
   published: {
      type: Boolean,
      default: false,
   },
   album: {
      type: mongoose.Schema.Types.ObjectId,
      required: [true, 'Album ID is required'],
      ref: 'albums',
   },
   duration: String,
   count: Number,
})

TrackSchema.plugin(idValidator);
const Track = mongoose.model('tracks', TrackSchema);
module.exports = Track;