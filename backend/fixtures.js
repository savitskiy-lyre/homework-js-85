const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const Post = require("./models/Post");
const Comment = require("./models/Comment");
const TrackHistory = require("./models/TrackHistory");


const run = async () => {
    await mongoose.connect(config.db.testStorageUrl)

    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [artist1, artist2, artist3] = await Artist.create({
            name: 'Беллами, Мэттью',
            published: true,
            photo: 'fixtures/artist1.jpg',
            description: 'Мэ́ттью Джеймс Бе́ллами (англ. Matthew James Bellamy; род. 9 июня 1978, Кембридж, Великобритания) — британский вокалист, гитарист, клавишник и композитор, фронтмен группы Muse.',
        }, {
            name: 'Уолстенхолм, Крис',
            published: true,
            photo: 'fixtures/artist2.jpg',
            description: 'Кристофер Тони Уолстенхолм (англ. Christopher Tony Wolstenholme; род. 2 декабря 1978, Ротерем, Саут-Йоркшир, Великобритания) — британский басист, бэк-вокалист и один из композиторов группы Muse. Кроме того, он иногда играет на гитаре вместо баса и играет на клавишных, но очень редко и только на живых выступлениях. Играет на электрогитаре, басу, клавишных, а также заменял ударника в одной из малоизвестных групп. На концерте на фестивале Rock Am Ring сыграл на губной гармошке. В клипе «Unintended» и на некоторых концертах во время исполнения этой же песни играл на контрабасе.',
        }, {
            name: 'Ховард, Доминик',
            photo: 'fixtures/artist3.jpg',
            description: 'Доминик Джеймс Ховард (англ. Dominic James Howard; род. 7 декабря 1977, Стокпорт, Англия) — барабанщик британской рок-группы Muse.',
        },
    )
    const [album1, album2, album3, album4] = await Album.create({
        title: 'Showbiz',
        author: artist1,
        date: '2005',
        image: 'fixtures/album1.jpg',
    }, {
        title: 'Origin of Symmetry',
        author: artist1,
        published: true,
        date: '2002',
        image: 'fixtures/album2.jpg',
    }, {
        title: 'Absolution',
        published: true,
        author: artist2,
        date: '1999',
        image: 'fixtures/album3.jpg',
    }, {
        title: 'Black Holes and Revelations',
        published: true,
        author: artist3,
        date: '2023',
    })
    const [track1, track2, track3] = await Track.create({
        title: 'Sunburn',
        album: album1,
        published: true,
        duration: '3:54',
        count: 5,
    }, {
        title: 'Muscle Museum',
        album: album1,
        duration: '4:23',
        count: 4,
    }, {
        title: 'Fillip',
        album: album1,
        published: true,
        duration: '2:01',
        count: 3,
    }, {
        title: 'Fillip Museum',
        album: album1,
        published: true,
        duration: '1:03',
        count: 2,
    }, {
        title: 'Muscle Fillip',
        album: album1,
        duration: '7:06',
        count: 1,
    }, {
        title: 'Uprising',
        album: album2,
        duration: '5:03',
        published: true,
        count: 5,
    }, {
        title: 'Resistance',
        album: album2,
        duration: '5:46',
        published: true,
        count: 4,
    }, {
        title: 'Undisclosed Desires',
        album: album2,
        duration: '3:56',
        published: true,
        count: 3,
    }, {
        title: 'United States of Eurasia / Collateral Damage',
        album: album2,
        duration: '5:47',
        count: 2,
    }, {
        title: 'Guiding Light',
        album: album2,
        duration: '6:54',
        count: 1,
    },{
        title: 'MK Ultra',
        album: album3,
        duration: '4:54',
        published: true,
        count: 5,
    }, {
        title: 'I Belong to You / Mon Cœur S’ouvre à ta Voix',
        album: album3,
        duration: '5:38',
        published: true,
        count: 4,
    }, {
        title: 'Exogenesis: Symphony Part I (Overture)',
        album: album3,
        duration: '4:18',
        published: true,
        count: 3,
    }, {
        title: 'Exogenesis: Symphony Part II (Cross Pollination)',
        album: album3,
        duration: '3:56',
        count: 2,
    }, {
        title: '3:56',
        album: album3,
        duration: '4:37',
        count: 1,
    },{
        title: 'Drill Sergeant',
        album: album4,
        duration: '0:21',
        published: true,
        count: 5,
    }, {
        title: 'Psycho',
        album: album4,
        duration: '5:16',
        published: true,
        count: 4,
    }, {
        title: 'Mercy',
        album: album4,
        duration: '3:51',
        published: true,
        count: 3,
    }, {
        title: 'Reapers',
        album: album4,
        duration: '5:59',
        count: 2,
    }, {
        title: 'The Handler',
        album: album4,
        duration: '4:33',
        count: 1,
    },)
    const [user1, user2] = await User.create({
        username: 'user',
        password: 'user',
        email: 'user@gmail.com',
        token: nanoid(),
    }, {
        username: 'admin',
        password: 'admin',
        email: 'admin@gmail.com',
        role: 'admin',
        token: nanoid(),
    })
    const [trackH1, trackH2, trackH3] = await TrackHistory.create({
        user: user1,
        track: track3,
        datetime: '2021-11-18T08:33:00.218Z',
    }, {
        user: user1,
        track: track1,
        datetime: '2020-11-18T08:33:00.218Z',
    }, {
        user: user1,
        track: track2,
        datetime: '2022-11-18T08:33:00.218Z',
    })
    const [post1, post2] = await Post.create({
        user: user1,
        title: 'teapot',
        image: 'fixtures/post1.jpg',
        description: '418 I’m a teapot — Этот код был введен в 1998 году как одна из традиционных первоапрельских шуток IETF в RFC 2324, Hyper Text Coffee Pot Control Protocol. Не ожидается, что данный код будет поддерживаться реальными серверами[21].',
    }, {
        user: user2,
        title: '412',
        description: '412 Precondition Failed — возвращается, если ни одно из условных полей заголовка (If-Match и др., см. RFC 7232) запроса не было выполнено. ',
    })
    const [comment1, comment2, comment3] = await Comment.create({
        user: user1,
        post: post1,
        text: 'Great',
    }, {
        user: user1,
        post: post1,
        text: 'Wow',
    }, {
        user: user2,
        post: post1,
        text: 'mehh',
    })

    await mongoose.connection.close();
}
run().catch(console.error)
