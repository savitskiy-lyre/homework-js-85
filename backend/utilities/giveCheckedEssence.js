const giveCheckedEssence = (obj) => {
   const checked = {};
   Object.keys(obj).forEach((key) => {
      if (obj[key]) checked[key] = obj[key];
   })
   return checked;
};

module.exports = giveCheckedEssence;