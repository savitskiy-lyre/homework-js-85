const express = require('express');
const router = express.Router();
const TrackHistory = require('../models/TrackHistory');
const Track = require('../models/Track');
const authUserToken = require("../middleware/authUserToken");

router.get('/', authUserToken, async (req, res) => {
    try {
        const trackHistory = await TrackHistory
            .find({user: req.user._id})
            .populate('user', 'username')
            .populate('track', 'title')
            .sort({datetime: -1});
        res.send(trackHistory);
    } catch (err) {
        console.log(err);
        res.status(500).send({error: 'Internal Server Error'});
    }
})

router.post('/', authUserToken, async (req, res) => {
    const {track_id} = req.body;
    if (!track_id) return res.status(400).send({error: "Track cannot be found without ID"});
    try {
        const track = await Track.findById(track_id);
        if (!track) return res.status(404).send({error: 'Track not found'});
        const trackHistory = new TrackHistory({user: req.user._id, track: track._id});
        await trackHistory.save();
        res.send(trackHistory);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});

module.exports = router;