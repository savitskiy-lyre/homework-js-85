const express = require('express');
const Comment = require("../models/Comment");
const authUserToken = require("../middleware/authUserToken");
const router = express.Router();

router.get('/', async (req, res) => {
    if (req.query.post) {
        try {
            const comments = await Comment
                .find({post: req.query.post})
                .populate('user', 'username');
            res.send(comments);
        } catch (err) {
            if (err?.kind === 'ObjectId') return res.status(400).send({error: 'Bad request'});
            res.status(500).send({error: 'Internal Server Error'});
        }
    } else {
        try {
            const comments = await Comment
                .find()
                .populate('user', 'username');
            res.send(comments);
        } catch (err) {
            res.status(500).send({error: 'Internal Server Error'});
        }
    }
});

router.post('/', authUserToken, async (req, res) => {
    const {text, post} = req.body;
    const comment = new Comment({user: req.user, text, post});
    try {
        await comment.save();
        res.send(comment);
    } catch (err) {
        if (err?.errors) {
            for (let key of Object.keys(err.errors)) {
                if (err.errors[key].path === 'text' || err.errors[key].path === 'user' || err.errors[key].path === 'post') {
                    return res.status(400).send({error: err.errors[key].message});
                }
            }
        }
        res.status(500).send({error: 'Internal Server Error'});
    }
});

module.exports = router;