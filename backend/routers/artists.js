const express = require('express');
const router = express.Router();
const Artist = require('../models/Artist');
const giveCheckedEssence = require('../utilities/giveCheckedEssence');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const permit = require("../middleware/permit");
const authUserToken = require("../middleware/authUserToken");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const artists = await Artist.find();
        res.send(artists);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});
router.post('/', authUserToken, permit('user', 'admin'), upload.single('photo'), async (req, res) => {
    const {name, description} = req.body;
    const newArtist = giveCheckedEssence({name, description});
    if (req.file) newArtist.photo = 'uploads/' + req.file.filename;
    const artist = new Artist(newArtist);
    try {
        await artist.save();
        res.send(artist);
    } catch (err) {
        if (err?.errors) {
            return res.status(400).send(err)
        }
        res.status(500).send({error: 'Internal Server Error'});
    }
});
router.put('/:id/publish', authUserToken, permit('admin'), async (req, res) => {
    const {id} = req.params;
    const {published: reqBool} = req.body;
    if (reqBool !== true && reqBool !== false) return res.status(401).send({error: 'Field "published" required boolean'});
    try {
        const artist = await Artist.findById(id);
        if (!artist) return res.status(404).send({error: 'Can\'t find artist for publish him'});
        artist.published = reqBool;
        await artist.save();
        res.send(artist);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});
router.delete('/:id', authUserToken, permit('admin'), async (req, res) => {
    const {id} = req.params;
    try {
        const artist = await Artist.findByIdAndDelete(id);
        if (!artist) res.status(404).send({error: 'Not found'});
        res.send({message: 'Success'});
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});

module.exports = router;