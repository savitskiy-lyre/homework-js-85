const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const router = express.Router();
const Album = require('../models/Album');
const authUserToken = require("../middleware/authUserToken");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    if (req.query.artist) {
        try {
            const artists = await Album
                .find({author: req.query.artist})
                .populate('author', 'name')
                .sort('date -test')

            res.send(artists);
        } catch (err) {
            res.status(500).send({error: 'Internal Server Error'});
        }
    } else {
        try {
            const artists = await Album
                .find()
                .populate('author', 'name')
                .sort('date -test');
            res.send(artists);
        } catch (err) {
            res.status(500).send({error: 'Internal Server Error'});
        }
    }
});
router.get('/:id', async (req, res) => {
    try {
        const artists = await Album
            .findById(req.params.id)
            .populate('author')
            .sort('date -test');
        res.send(artists);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

router.post('/', authUserToken, permit('admin', 'user'), upload.single('image'), async (req, res) => {
    const {title, author, date} = req.body;
    const createdAlbum = {title, author, date};
    if (req.file) createdAlbum.image = 'uploads/' + req.file.filename;
    const album = new Album(createdAlbum);
    try {
        await album.save();
        res.send(album);
    } catch (err) {
        if (err?.errors) {
            return  res.status(400).send(err)
        }
        res.status(500).send({error: 'Internal Server Error'});
    }
});
router.put('/:id/publish', authUserToken, permit('admin'), async (req, res) => {
    const {id} = req.params;
    const {published: reqBool} = req.body;
    if (reqBool !== true && reqBool !== false) return res.status(401).send({error: 'Field "published" required boolean'});
    try {
        const artist = await Album.findById(id);
        if (!artist) return res.status(404).send({error: 'Can\'t find album for publish him'});
        artist.published = reqBool;
        await artist.save();
        res.send(artist);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});
router.delete('/:id', authUserToken, permit('admin'), async (req, res) => {
    const {id} = req.params;
    try {
        const artist = await Album.findByIdAndDelete(id);
        if (!artist) res.status(404).send({error: 'Not found'});
        res.send({message: 'Success'});
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});
module.exports = router;