const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require("nanoid");
const config = require("../config");
const authUser = require('../middleware/authUser');
const User = require('../models/User');
const axios = require("axios");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


router.post('/', upload.single('image'), async (req, res) => {
    const {username, password, email} = req.body;
    const userData = {username, password, email};
    if (req.file) userData.image = '/uploads/' + req.file.filename;
    const user = new User(userData);

    try {
        user.generateToken();
        await user.save();
        res.send(user);

    } catch (err) {
        if (err?.errors) {
            for (let key of Object.keys(err.errors)) {
                if (err.errors[key].path === 'username' || err.errors[key].path === 'password' || err.errors[key].path === 'email') {
                    return res.status(400).send({error: err.errors[key].message});
                }
            }
        }
        res.status(500).send({error: 'Internal Server Error'});
    }
});

router.post('/sessions', authUser, async (req, res) => {
    try {
        req.user.generateToken();
        await req.user.save({validateBeforeSave: false});
        res.send(req.user);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + "|" + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
    try {
        const {data} = await axios.get(debugTokenUrl);

        if (data.data.error) return res.status(401).send({global: 'Facebook token incorrect!'});
        if (req.body.id !== data.data.user_id) return res.status(401).send({global: 'Wrong user id'});

        let user = await User.findOne({email: req.body.email});

        if (!user) {
            user = await User.findOne({facebookId: req.body.id});
        }

        if (!user) {
            user = new User({
                email: req.body.email || nanoid(),
                password: nanoid(),
                facebookId: req.body.id,
                username: req.body.name,
                image: req.body.picture.data.url || null,
            });
        }

        user.generateToken();
        user.save({validateBeforeSave: false});

        res.send(user);
    } catch (err) {
        res.status(401).send({error: 'Facebook token incorrect!'})
    }
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Success'};
    if (!token) return res.send(success);
    const user = await User.findOne({token});
    if (!user) return res.send(success);
    try {
        user.generateToken();
        await user.save({validateBeforeSave: false});
        return res.send(success);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }

});

module.exports = router;