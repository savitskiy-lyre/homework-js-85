const express = require('express');
const authUserToken = require("../middleware/authUserToken");
const giveCheckedEssence = require("../utilities/giveCheckedEssence");
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const Post = require("../models/Post");
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const posts = await Post
            .find()
            .populate('user', 'username')
            .sort('datetime test');
        res.send(posts);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});

router.get('/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const post = await Post
            .findById(id)
            .populate('user', 'username');
        res.send(post);
    } catch (err) {
        if (err?.kind === 'ObjectId') return res.status(400).send({error: 'Bad request'});
        res.status(500).send({error: 'Internal Server Error'});
    }
});

router.post('/', [authUserToken, upload.single('image')], async (req, res) => {
    const {title, description} = req.body;
    const newPost = giveCheckedEssence({title, description, user: req.user._id});
    if (req.file) newPost.image = 'uploads/' + req.file.filename;
    const post = new Post(newPost);
    try {
        await post.save();
        res.send(post);
    } catch (err) {
        if (err?.errors) {
            for (let key of Object.keys(err.errors)) {
                if (err.errors[key].path === 'title' || err.errors[key].path === 'user') {
                    return res.status(400).send({error: err.errors[key].message});
                }
            }
        }
        res.status(500).send({error: 'Internal Server Error'});
    }
});

module.exports = router;