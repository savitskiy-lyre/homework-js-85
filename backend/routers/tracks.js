const express = require('express');
const router = express.Router();
const giveCheckedEssence = require('../utilities/giveCheckedEssence');
const Track = require('../models/Track');
const authUserToken = require("../middleware/authUserToken");
const permit = require("../middleware/permit");

router.get('/', async (req, res) => {
    if (req.query.album) {
        try {
            const tracks = await Track
                .find({album: req.query.album})
                .populate({
                    path: 'album',
                    populate: {
                        path: 'author',
                    },
                })
                .sort('count -test');
            res.send(tracks);
        } catch (err) {
            res.status(500).send({error: 'Internal Server Error'});
        }
    } else {
        try {
            const artists = await Track
                .find()
                .populate({
                    path: 'album',
                    populate: {
                        path: 'author',
                    },
                })
                .sort('count -test');
            res.send(artists);
        } catch (err) {
            res.status(500).send({error: 'Internal Server Error'});
        }
    }
});

router.post('/', authUserToken, permit('user', 'admin'), async (req, res) => {
    const {title, album, duration, count} = req.body;
    const track = new Track(giveCheckedEssence({title, album, duration, count}));
    try {
        await track.save();
        res.send(track);
    } catch (err) {
        if (err?.errors) {
            return res.status(400).send(err)
        }
        res.status(500).send({error: 'Internal Server Error'});
    }
});
router.put('/:id/publish', authUserToken, permit('admin'), async (req, res) => {
    const {id} = req.params;
    const {published: reqBool} = req.body;
    if (reqBool !== true && reqBool !== false) return res.status(401).send({error: 'Field "published" required boolean'});
    try {
        const artist = await Track.findById(id);
        if (!artist) return res.status(404).send({error: 'Can\'t find track for publish him'});
        artist.published = reqBool;
        await artist.save();
        res.send(artist);
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});
router.delete('/:id', authUserToken, permit('admin'), async (req, res) => {
    const {id} = req.params;
    try {
        const artist = await Track.findByIdAndDelete(id);
        if (!artist) res.status(404).send({error: 'Not found'});
        res.send({message: 'Success'});
    } catch (err) {
        res.status(500).send({error: 'Internal Server Error'});
    }
});
module.exports = router;