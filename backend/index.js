require('dotenv').config()
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const artists = require('./routers/artists');
const albums = require('./routers/albums');
const tracks = require('./routers/tracks');
const users = require('./routers/users');
const posts = require('./routers/posts');
const comments = require('./routers/comments');
const tracksHistories = require('./routers/tracksHistories');
const config = require('./config');

const server = express();
server.use(express.json());
server.use(express.static('public'));
server.use(cors());
const port = 7000;

server.get('/', (req, res) => {
    res.send('Hello')
})

server.use('/artists', artists);
server.use('/albums', albums);
server.use('/tracks', tracks);
server.use('/users', users);
server.use('/posts', posts);
server.use('/comments', comments);
server.use('/tracksHistories', tracksHistories);

const run = async () => {
    await mongoose.connect(config.db.testStorageUrl);

    server.listen(port, () => {
        console.log(`Server is started on ${port} port !`);
    })

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
}
run().catch(e => console.error(e));